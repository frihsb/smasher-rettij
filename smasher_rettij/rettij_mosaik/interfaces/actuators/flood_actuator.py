from .abstract_actuator import AbstractActuator


class FloodActuator(AbstractActuator):
    flood_list = []  # stores node name to make reversion of flooding possible

    @staticmethod
    def get_name() -> str:
        return "flood"

    @staticmethod
    def get_action_space() -> str:
        return "Box(low=0, high=1, shape=(1,), dtype=np.float32)"

    @classmethod
    def add_to_list(cls, node):
        cls.flood_list.append(node)

    @classmethod
    def remove_from_list(cls, node):
        cls.flood_list.remove(node)

    def execute(self, attr_value):
        if attr_value:
            if self._node.name not in self.flood_list:
                print(f"Flooding node {self._node.name}")
                for iface_name, iface in self._node.ifaces.items():
                    iface.channel.data_rate.rate = iface.channel.data_rate.rate * 0.1
                    iface.channel.delay = str(float(iface.channel.delay.replace("ms", "")) * 10) + "ms"
                    self.add_to_list(self._node.name)
                    print(f"new datarate: {iface.channel.data_rate.rate}; delay: {iface.channel.delay}")
            else:
                print(f"Un-flooding node {self._node.name}")
                for iface_name, iface in self._node.ifaces.items():
                    iface.channel.data_rate.rate = iface.channel.data_rate.rate * 10
                    iface.channel.delay = str(float(iface.channel.delay.replace("ms", "")) / 10) + "ms"
                    self.remove_from_list(self._node.name)
                    print(f"new data rate: {iface.channel.data_rate.rate}; delay: {iface.channel.delay}")
