import time

from .abstract_actuator import AbstractActuator


class DowntimeActuator(AbstractActuator):
    @staticmethod
    def get_name() -> str:
        return "downtime"

    @staticmethod
    def get_action_space() -> str:
        return "Box(low=0, high=1, shape=(1,), dtype=np.float32)"

    def execute(self, attr_value):
        if attr_value:
            print(f"Taking node {self._node.name} down for maintenance")
            for iface_name, iface in self._node.ifaces.items():
                iface.down()
            time.sleep(15)
            print(f"Starting node {self._node.name} after maintenance")
            for iface_name, iface in self._node.ifaces.items():
                iface.up()
