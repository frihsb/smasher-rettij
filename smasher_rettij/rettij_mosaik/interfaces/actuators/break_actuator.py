from .abstract_actuator import AbstractActuator


class BreakActuator(AbstractActuator):
    @staticmethod
    def get_name() -> str:
        return "break"

    @staticmethod
    def get_action_space() -> str:
        return "Box(low=0, high=1, shape=(1,), dtype=np.float32)"

    def execute(self, attr_value):
        if attr_value:
            print(f"Breaking node {self._node.node_id}")
            # TODO call rettij API
