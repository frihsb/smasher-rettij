from abc import abstractmethod, ABC

from ..abstract_interface import AbstractInterface


class AbstractActuator(AbstractInterface):
    @staticmethod
    def get_type() -> str:
        return "actuator"

    @staticmethod
    @abstractmethod
    def get_action_space() -> str:
        ...

    @abstractmethod
    def execute(self, attr_value):
        """Execute actuator.
        :param attr_value: Value of mosaik attribute representing this actuator
        :return: None
        """
        ...
