from typing import List

from .reboot_actuator import RebootActuator
from .break_actuator import BreakActuator
from .downtime_actuator import DowntimeActuator
from .flood_actuator import FloodActuator
from ..abstract_interface_factory import AbstractInterfaceFactory


class ActuatorFactory(AbstractInterfaceFactory):
    @staticmethod
    def get_type() -> str:
        return "actuator"

    def get_interface_classes(self) -> List:
        actuator_classes = [RebootActuator, BreakActuator, DowntimeActuator, FloodActuator]
        return actuator_classes
