### How to implement new actuators / sensors

Actuators and sensors are both "interfaces" used in the co-simulation to interact with other simulators, thus you'll find the word "interface" used to wrap up those two words. Don't confuse it with a "network interfaces" which would be a completely different thing.

Implementing a new actuator or sensor is quite straight forward:

1. Create a new file in the `actuators` / `sensors` directory (depending on which type of interface you want to implement) and name it with the class name (just take a look at the existing interfaces first).
1. Inherit from the `AbstractActuator` / `AbstractSensor` class.
1. Implement the abstract methods like `get_name()` and `execute()` (in case it's an actuator) or `read()` (in case it's a sensor).
1. In case there are e.g. multiple sensors per node, you have to override the `gen_eids()`-method as well (as seen in the interface utilization sensor which has multiple eids / sensors per node because every node can have multiple interfaces)
1. Add the new sensors / actuators to TODO TODO TODO