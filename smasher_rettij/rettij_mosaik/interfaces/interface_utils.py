import re


class InterfaceUtils:
    """
    An Interface is the summarizing term for both actuators and sensors.

    This class has some helper utils for working with interfaces.
    """

    # compile regex here (not in init()) and just once for better performance
    # example eid to be parsed: 'market-agent-model-0/sensor-interface-utilization/iface-i0'
    __eid_regex_pattern = (
        "^(?P<node_id>[a-zA-Z\\d\\-]+)/?(?:(?P<interface_type>actuator|sensor)-(" "?P<interface_name>[\\S]+))?$"
    )
    __eid_matcher = re.compile(__eid_regex_pattern)

    @staticmethod
    def parse_eid(eid):
        parsed_values = {}
        match = InterfaceUtils.__eid_matcher.match(eid)
        if not match:
            return None
        parsed_values["node_id"] = match.group("node_id")
        parsed_values["interface_type"] = match.group("interface_type")
        parsed_values["interface_name"] = match.group("interface_name")
        return parsed_values
