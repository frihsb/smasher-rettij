from typing import List

from .abstract_sensor import AbstractSensor


class InterfaceUtilizationSensor(AbstractSensor):
    @staticmethod
    def get_name() -> str:
        return "interface-utilization"

    @staticmethod
    def get_action_space() -> str:
        return "Box(low=0, high=1, shape=(1,), dtype=np.float32)"

    @classmethod
    def gen_eids(cls, rettij):
        """Generate all possible eids for this sensor, e.g. for each node's network-interface one."""
        sensor_eids = {}
        for node_name, node in rettij.nodes.items():
            interface_names: List[str] = node.ifaces.keys()
            sensor_eids[node_name] = []
            for iface in interface_names:
                eid = f"{node.name}/sensor-{cls.get_name()}/iface-{iface}"
                sensor_eids[node_name].append(eid)
        return sensor_eids

    def read(self):
        iface_name = self._eid[len(f"{self._node.name}/sensor-{self.get_name()}/iface-") :]
        return self._node.ifaces[iface_name].utilization()["total"]  # return total transmission (rx + tx) in bytes/s
