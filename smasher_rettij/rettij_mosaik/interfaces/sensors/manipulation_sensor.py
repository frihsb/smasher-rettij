from typing import List

from .abstract_sensor import AbstractSensor


class ManipulationSensor(AbstractSensor):
    @staticmethod
    def get_name() -> str:
        return "manipulation"

    @staticmethod
    def get_action_space() -> str:
        return "Box(low=0, high=1, shape=(1,), dtype=np.float32)"

    @classmethod
    def gen_eids(cls, rettij):
        """Generate all possible eids for this sensor, e.g. for each other sensor on node."""
        # TODO: implement EID generation for more than interface-utilization sensors.
        sensor_eids = {}
        for node_name, node in rettij.nodes.items():
            interface_names: List[str] = node.ifaces.keys()
            sensor_eids[node_name] = []
            for iface in interface_names:
                eid = f"{node.name}/sensor-{cls.get_name()}/interface-utilization/iface-{iface}"
                sensor_eids[node_name].append(eid)
        return sensor_eids

    def read(self):
        target = (
            self._eid.split("manipulation/")[0] + self._eid.split("manipulation/")[1]
        )  # takes own name out of eid to get targeted eid
        offset = 10  # right now this is an arbitrary value. for an offset it needs to be changed in the rettij_mosaik_pyrate/get_data method
        return {"target": target, "offset": offset}
