from typing import List

from .interface_utilization_sensor import InterfaceUtilizationSensor
from .manipulation_sensor import ManipulationSensor
from ..abstract_interface_factory import AbstractInterfaceFactory


class SensorFactory(AbstractInterfaceFactory):
    @staticmethod
    def get_type() -> str:
        return "sensor"

    def get_interface_classes(self) -> List:
        sensor_classes = [
            InterfaceUtilizationSensor,
            ManipulationSensor,
            # ReachableSensor,
        ]
        return sensor_classes
