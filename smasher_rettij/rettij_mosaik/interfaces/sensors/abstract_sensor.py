from abc import abstractmethod, ABC

from ..abstract_interface import AbstractInterface


class AbstractSensor(AbstractInterface):
    @staticmethod
    def get_type() -> str:
        return "sensor"

    @staticmethod
    @abstractmethod
    def get_action_space() -> str:
        ...

    @abstractmethod
    def read(self):
        ...
