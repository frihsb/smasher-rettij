from abc import abstractmethod, ABC


class AbstractInterface(ABC):
    def __init__(self, eid, node, rettij):
        self._eid = eid
        self._node = node
        self._rettij = rettij

    @staticmethod
    @abstractmethod
    def get_name() -> str:
        ...

    @staticmethod
    @abstractmethod
    def get_type() -> str:
        """Return string "actuator" or "sensor", depending on the type of the interface."""
        ...

    @staticmethod
    @abstractmethod
    def get_action_space() -> str:
        ...

    @classmethod
    def gen_eids(cls, rettij):
        interface_eids = {}
        for node_id in rettij.nodes:
            eid = f"{node_id}/{cls.get_type()}-{cls.get_name()}"
            # list because it can be multiple actuators / sensors per node sometimes
            interface_eids[node_id] = [eid]
        return interface_eids
