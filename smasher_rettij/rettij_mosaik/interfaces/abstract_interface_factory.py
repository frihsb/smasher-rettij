from abc import abstractmethod, ABC

from typing import List, Dict, Optional

from .abstract_interface import AbstractInterface


class AbstractInterfaceFactory(ABC):
    def __init__(self, rettij):
        self.__rettij = rettij
        self.__node2eid = {}
        self.__interfaces = {}
        # init all interface (=actuator and sensor) entity ids as well as class instances
        for interface_class in self.get_interface_classes():
            eids: Dict[str, List] = interface_class.gen_eids(self.__rettij)
            for node_name, interfaces in eids.items():
                self.__node2eid.setdefault(node_name, []).extend(interfaces)
            self.__interfaces.update(self.__create_interfaces(interface_class))

    @staticmethod
    @abstractmethod
    def get_type() -> str:
        ...

    @abstractmethod
    def get_interface_classes(self):
        ...

    @property
    def eids(self):
        return self.__node2eid

    def get(self, eid) -> Optional[AbstractInterface]:
        try:
            return self.__interfaces[eid]
        except KeyError:
            return None

    def __create_interfaces(self, interface_class):
        all_eids = interface_class.gen_eids(self.__rettij)
        interfaces = {}
        for node in self.__rettij.nodes.values():
            node_eids = all_eids[node.name]
            for eid in node_eids:
                interfaces[eid] = interface_class(eid, node, self.__rettij)
        return interfaces
