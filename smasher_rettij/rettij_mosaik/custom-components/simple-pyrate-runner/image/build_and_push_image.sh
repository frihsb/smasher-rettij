#!/bin/bash

# Purpose: Build image and push it to a docker registry

repository="frihsb/simple-pyrate-runner"
tag="20220718"

docker build -t "$repository":$tag .
docker push "$repository":$tag