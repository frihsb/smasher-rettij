import http.server
import json
import logging
import threading
import http.client
from functools import partial
from typing import Dict, Any, List, Optional

logger = logging.getLogger(__name__)


class MosaikServerThread(threading.Thread):
    """
    Thread for starting server to receive and answer GET and POST requests from mosaik.

    This is used to get data out of the rettij simulation into mosaik.
    """
    def __init__(self,
                 host: str,
                 mosaik_listen_port: int,
                 rettij_target_port: int,
                 targets_file_path: str,
                 data_storage: Dict[str, Any],
                 data_storage_lock: threading.Lock
                 ) -> None:
        super().__init__()
        self.__host: str = host
        self.__mosaik_listen_port: int = mosaik_listen_port
        self.__rettij_target_port: int = rettij_target_port
        self.__targets_file_path: str = targets_file_path
        self.data_storage = data_storage
        self.data_storage_lock = data_storage_lock
        logger.debug(f"Initialising server thread for host '{self.__host}'...")

    def run(self) -> None:
        settings = {"rettij_target_port": self.__rettij_target_port, "targets_file_path": self.__targets_file_path}
        logger.debug("Starting mosaik server...")
        handler = partial(MosaikDataHandler, settings, self.data_storage, self.data_storage_lock)
        # start http server
        with http.server.HTTPServer((self.__host, self.__mosaik_listen_port), handler) \
                as mosaik_server:
            mosaik_server.serve_forever()
            logger.debug("Stopping mosaik server...")


class MosaikDataHandler(http.server.BaseHTTPRequestHandler):
    """
    Server that runs in the simple-pyrate-runner pod to send data to the outside world from mosaik to rettij
    by the step() method and from rettij to mosaik with the get_data() method.

    There will only be a single rettij instance that connects via this "management port" on multiple pods. So each pod
    only needs to handle a single incoming connection. (keep in mind, that Python would instantiate one instance of
    this RequestHandler class per incoming connection to the server)
    """
    def __init__(self, settings: Dict[str, Any], data_storage: Dict[str, Any], data_storage_lock: threading.Lock,
                 *args, **kwargs):
        self.__rettij_target_port: Optional[int] = settings["rettij_target_port"]
        self.__targets_file_path: str = settings["targets_file_path"]
        self.data_storage: Dict[str, Any] = data_storage
        self.data_storage_lock = data_storage_lock

        logger.debug(f"Initialising MosaikDataHandler with settings '{settings}'...")

        # BaseHTTPRequestHandler calls do_GET() and do_POST() *inside* __init__() already!
        # So we have to call super().__init__() *after* setting all of the above attributes!
        super().__init__(*args, **kwargs)

    def do_GET(self) -> None:
        """
        Handle incoming GET request from mosaik get_data() method.
        """
        logger.debug("GET | Handling incoming request...")
        self.data_storage_lock.acquire()
        data = json.dumps(self.data_storage).encode("utf-8")
        self.data_storage_lock.release()
        length = len(data)
        self.protocol_version = 'HTTP/1.1'
        self.send_response(200, 'OK')
        self.send_header('Content-type', 'application/json')
        self.send_header('Content-length', str(length))
        self.end_headers()
        logger.debug(f"GET | Sending data: {data}")
        self.wfile.write(data)
        logger.debug(f"GET | Sent data to mosaik: {data}")

    def do_POST(self) -> None:
        """
        Handle incoming POST request from mosaik step() method that sets the data on the node.

        The attributes and their values received from mosaik are forwarded / sent to the according rettij target nodes.
        The target node IPs for each attribute are defined by calls to the connect() hook at the start of the
        simulation.
        """
        logger.debug("POST | Handling incoming request...")
        length = int(self.headers['Content-length'])
        json_in_data = self.rfile.read(length)
        logger.debug(f"POST | Received raw data: {json_in_data}")
        try:
            in_data = json.loads(json_in_data)
            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
        except json.JSONDecodeError as e:
            logger.error(f"POST | Could not decode received JSON data: {e}")
            return
        except Exception as e:
            logger.error(e)
            return
        logger.debug(f"POST | Parsed incoming request: {in_data}")

        targets: Dict[str, List[str]] = self.load_targets()
        logger.debug(f"POST | Iterating over data: {in_data.items()}")
        for attr_name, attr_val in in_data.items():
            logger.debug(f"POST | Processing '{attr_name}', {attr_val}...")
            target_ip_list: List[str] = targets.get(attr_name, [])  # get list of target IPs or return empty list
            logger.debug(f"POST | Target IP list: {target_ip_list}")
            for target_ip in target_ip_list:
                data: Dict[str, Any] = {attr_name: attr_val}
                json_data = json.dumps(data)
                try:
                    logger.debug(f"POST | Sending data via rettij network to target node at "
                                 f"'{target_ip}:{self.__rettij_target_port}': {json_data}")
                    conn = http.client.HTTPConnection(target_ip, self.__rettij_target_port, timeout=10)
                    conn.request("POST", "", json_data)
                    resp = conn.getresponse()
                    if resp.status != 200:
                        logger.error(
                            f"POST | Error occurred sending data via forwarded kubernetes port to node. Got http "
                            f"status '{resp}'. Expected '200'. "
                        )
                except Exception as e:
                    logger.error(f"POST | Could not send POST request: {e}")

    def load_targets(self) -> Dict[str, List[str]]:
        """
        Read targets JSON file and return the target nodes as a dict.
        """
        try:
            with open(self.__targets_file_path, "r") as target_file:
                logger.debug(f"Loading targets from {self.__targets_file_path}...")
                targets: dict = json.load(target_file)
                logger.debug(f"Loaded targets from {self.__targets_file_path}: {targets}")
                return targets
        except Exception as e:
            logger.error(f"Could not load targets file: {e}")
