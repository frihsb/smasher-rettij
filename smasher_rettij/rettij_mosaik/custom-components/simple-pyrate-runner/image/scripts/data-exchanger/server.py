#!/usr/bin/env python3
import logging
import os
import threading
import time
from typing import Dict

from rettij_server import RettijNetworkServerThread
from mosaik_server import MosaikServerThread

# create log directory
log_dir: str = os.path.dirname(__file__) + "/log"
os.makedirs(log_dir, exist_ok=True)
log_file_path: str = os.path.join(log_dir, f"server.log")  # construct log file path

# create and configure logger
logger: logging.Logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s | %(name)s | %(levelname)s | %(message)s")
fh: logging.FileHandler = logging.FileHandler(log_file_path)
fh.setLevel(logging.DEBUG)
fh.setFormatter(formatter)
logger.addHandler(fh)

# Storage for data coming from mosaik (via step() method) and going to mosaik get_data()
# This basically acts as an in memory database
data_storage: Dict[str, str] = {}
data_storage_lock: threading.Lock = threading.Lock()
# TODO thread safety

# server for communication mosaik step()/get_data() methods and the rettij node
mosaik_server_thread = MosaikServerThread(host="localhost",
                                          mosaik_listen_port=6312,
                                          rettij_target_port=9999,
                                          targets_file_path="/rettij/targets.json",
                                          data_storage=data_storage,
                                          data_storage_lock=data_storage_lock,
                                          )

# server for communication between rettij nodes (within the rettij network)
rettij_server_thread = RettijNetworkServerThread(host="0.0.0.0",
                                                 port=9999,
                                                 data_storage=data_storage,
                                                 data_storage_lock=data_storage_lock,
                                                 )
rettij_server_thread.start()
mosaik_server_thread.start()

rettij_server_thread.join()
mosaik_server_thread.join()
