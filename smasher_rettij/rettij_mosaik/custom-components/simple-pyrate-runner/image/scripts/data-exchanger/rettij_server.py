import http.server
import json
import threading
from functools import partial
from typing import Dict, Any
import logging

logger = logging.getLogger(__name__)


class RettijNetworkServerThread(threading.Thread):
    """
    Thread for starting a server to receive data from the internal rettij simulation network via POST requests.
    """
    def __init__(self, host: str, port: int, data_storage: Dict[str, Any], data_storage_lock: threading.Lock):
        self.__host: str = host
        self.__port: int = port
        self.data_storage = data_storage
        self.data_storage_lock = data_storage_lock
        logger.debug(f"Initialising rettij server thread for host '{self.__host}:{self.__port}'...")
        super().__init__()

    def run(self):
        # prepend data storage attribute to RettijDataHandler class __init__() method
        handler = partial(RettijDataHandler, self.data_storage, self.data_storage_lock)
        logger.debug(f"Running rettij server thread for host '{self.__host}:{self.__port}'...")
        # start http server
        with http.server.HTTPServer((self.__host, self.__port), handler) as rettij_network_server:
            logger.debug("Starting rettij server...")
            rettij_network_server.serve_forever()
            logger.debug("Stopping rettij server...")


class RettijDataHandler(http.server.BaseHTTPRequestHandler):
    def __init__(self, data_storage: Dict[str, Any], data_storage_lock: threading.Lock,  *args, **kwargs):
        logger.debug(f"Initialising RettijDataHandler...")
        self.data_storage = data_storage
        self.data_storage_lock = data_storage_lock
        super().__init__(*args, **kwargs)

    def do_POST(self):
        """
        Handle incoming POST request from different rettij node.

        The POST request is usually sent by the mosaik server on another rettij node. It forwards the attributes
        received from mosaik to this rettij server.
        """
        logger.debug("POST | Handling incoming POST request...")
        length = int(self.headers['Content-length'])
        json_in_data = self.rfile.read(length)
        logger.debug(f"POST | Received raw data: {json_in_data}")
        try:
            in_data = json.loads(json_in_data)
            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
        except json.JSONDecodeError as e:
            logger.error(f"POST | Could not decode received JSON data: {e}")
            return
        except Exception as e:
            logger.error(e)
            return
        logger.debug(f"POST | Parsed incoming request: {in_data}")

        self.data_storage_lock.acquire()
        self.data_storage = self.__merge_dicts(self.data_storage, in_data)
        logger.debug(f"POST | New storage dict: {self.data_storage}")
        self.data_storage_lock.release()

    @staticmethod
    def __merge_dicts(storage_dict: Dict[str, Any], received_data_dict: Dict[str, Any]):
        """
        Merge dict stored on node with received inputs.

        If a key (an attribute) contains a dict, that has to be
        updated instead of top-level dict, which would mean overwriting the dict accessed by the key.

        Example for use case 1:
        - Stored data:   {"q_offers": {"MarketAgents-0.MarketAgentModel_PV_0": 0}}
        - Received data: {"q_offers": {"MarketAgents-0.MarketAgentModel_PV_1": 1}}
        -> Result: {'q_offers': {'MarketAgents-0.MarketAgentModel_PV_0': 0, 'MarketAgents-0.MarketAgentModel_PV_1': 1}}

        Example for use case 2:
        - Stored data:   {"foo": 1, "test": 2}
        - Received data: {"bar": 2}
        -> Result: {'foo': 1, 'test': 2, 'bar': 3}
        """
        for key in received_data_dict:
            if key in storage_dict and isinstance(storage_dict[key], dict):
                storage_dict[key].update(received_data_dict[key])
            else:
                storage_dict[key] = received_data_dict[key]
        return storage_dict
