import logging
import os
from typing import Dict

from rettij.topology.hooks.abstract_pre_teardown_hook import (
    AbstractPreTeardownHook,
)
from rettij.topology.network_components.node import Node


class PreTeardownHook(AbstractPreTeardownHook):
    def execute(self, node: Node, nodes: Dict[str, Node]) -> None:
        log_dir: str = os.path.dirname(__file__) + "/log/"
        os.makedirs(log_dir, exist_ok=True)

        try:
            print(
                node.executor.copy_file_from_node(
                    "/data-exchanger/log/receiver.log",
                    f"{log_dir}/{node.name}",
                    "/data-exchanger/log/server.log", f"{log_dir}{node.name}"
                )
            )
        except RuntimeError as e:
            logging.error(str(e))


        print(f"Executed pre-teardown hook of node {node.name}.")
