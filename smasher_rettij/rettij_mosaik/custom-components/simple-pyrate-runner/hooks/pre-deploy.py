from typing import Dict

from rettij.topology.hooks.abstract_pre_deploy_hook import AbstractPreDeployHook
from rettij.topology.network_components.node import Node


class PreDeployHook(AbstractPreDeployHook):
    def execute(self, node: Node, nodes: Dict[str, Node]) -> None:
        pass
