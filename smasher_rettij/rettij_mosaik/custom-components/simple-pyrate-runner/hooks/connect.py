from typing import Any, Optional, Dict

from rettij.topology.hooks.abstract_connect_hook import AbstractConnectHook
from rettij.topology.network_components.node import Node


class ConnectHook(AbstractConnectHook):
    def execute(self, source_node: Node, target_node: Node, **kwargs: Any) -> None:
        """
        Establish an attribute based 'connection' from one simulation Node to another.

        Works by creating a file '/rettij/targets.json' containing mappings of a single attribute to multiple destination ip addresses. Example content: {"value": ["192.168.1.10"]}.
        :param source_node: Source Node for the connection
        :param target_node: Destination Node for connection
        :param kwargs: Parameters needed to generate the Pod configuration.
            - 'attribute' (str) - Attribute name, that matches the attributes exchanged between mosaik and rettij
            - 'target_interface_name' (Optional, str) - The name of the interface on the target node, as defined in the topology. Default: i0.
        """
        # Read the target.json from source node
        targets: Dict[str, Any] = source_node.executor.read_values(file="/rettij/targets.json")

        target_interface_name: str = kwargs.get("target_interface_name", "i0")

        target_ip: str = target_node.ifaces[target_interface_name].ip.compressed

        try:
            attribute_name: str = kwargs["attribute"]
        except KeyError:
            raise ValueError("Keyword argument 'attribute' is required.")

        try:
            targets[attribute_name] += [target_ip]
        except KeyError:
            targets[attribute_name] = [target_ip]

        print(f"Connect hook of node {source_node.name}, writes {targets}")

        source_node.executor.write_values(targets, file="/rettij/targets.json")
