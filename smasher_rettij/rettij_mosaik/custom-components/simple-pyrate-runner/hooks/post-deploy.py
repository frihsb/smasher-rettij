from typing import Dict

import rettij.topology.node_executors.kubernetes_pod_executor
from rettij.topology.hooks.abstract_post_deploy_hook import (
    AbstractPostDeployHook,
)
from rettij.topology.network_components.node import Node


class PostDeployHook(AbstractPostDeployHook):
    def execute(self, node: Node, nodes: Dict[str, Node]) -> None:
        node.run(
            "python3 /data-exchanger/server.py",
            detached=True,
        )

        print(f"Executed post-deploy hook of node {node.name}.")
