import json
import logging
import os
from pathlib import Path
import sys
from typing import Callable, List, Any, Dict, Optional, Union
import rettij
from rettij.common.constants import COMPONENTS_DIR
from rettij.common.logging_utilities import Loglevel

from rettij.topology.network_components.node import Node

from .interfaces.actuators.actuator_factory import (
    ActuatorFactory,
)
from .interfaces.interface_utils import (
    InterfaceUtils,
)
from .interfaces.sensors.sensor_factory import (
    SensorFactory,
)

from .interfaces.abstract_interface import AbstractInterface

from rettij.cosim.rettij_mosaik import RettijMosaik


rettij_meta = {
    "api_version": "3.0",  # compatible with mosaik API version
    "type": "time-based",  # https://mosaik.readthedocs.io/en/latest/scheduler.html#stepping-types
    "models": {
        "Rettij": {
            "public": True,
            "params": [],  # currently, rettij_mosaik has no model params (only sim params)
            "attrs": [],
        },
        "node": {
            "public": False,
            "params": [],
            "attrs": [],  # these are extended in init() automatically from the topology "data" keys for the nodes
        },
        "actuator": {
            "public": False,
            "params": [],
            "attrs": ["value"],
        },
        "sensor": {
            "public": False,
            "params": [],
            "attrs": ["value"],
        },
    },
    "extra_methods": [
        "connect",
        "get_actuator",
        "get_sensor",
    ],
}

logger = logging.getLogger(__name__)


class SimRettijPyrate(RettijMosaik):
    def __init__(self):
        super().__init__(rettij_meta)
        self.__actuator_factory = None
        self.__sensor_factory = None

    def init(
        self,
        sid: str,
        topology_path: Union[Path, str],
        file_loglevel: Loglevel = Loglevel.NOTSET,
        console_loglevel: Loglevel = Loglevel.NOTSET,
        fail_on_step_error: bool = True,
        monitoring_config_path: Optional[Union[Path, str]] = None,
        sequence_path: Optional[Union[Path, str]] = None,
        kubeconfig_path: Optional[Union[Path, str]] = None,
        components_dir_path: Optional[Union[Path, str]] = Path(COMPONENTS_DIR),
        mermaid_export_path: Optional[Union[Path, str]] = None,
        step_size: int = 1,
        callback: Optional[Callable[[], None]] = None,
        rettij_instance: Optional[rettij.Rettij] = None,
        time_resolution: float = 1.0,
    ) -> Any:
        """
        Initialize the SimRettij mosaik simulator and rettij.

        Implements mosaik `init()` method.

        :param sid: Simulator id.
        :param topology_path: Path to the file containing the topology.
        :param file_loglevel: (Optional) File loglevel. Default: NOTSET (controlled externally by midas)
        :param console_loglevel: (Optional) Console loglevel. Default: NOTSET (controlled externally by midas)
        :param fail_on_step_error: (Optional) If True, raise exception on step execution failure. If False, continue execution.
        :param monitoring_config_path: (Optional) Path to the InfluxDB monitoring logging config file.
        :param sequence_path: (Optional) Path to the file containing the simulation sequence.
        :param kubeconfig_path: (Optional) Path to the file containing the `kubeconfig` file.
        :param components_dir_path: (Optional) Path to the directory containing custom components. Default: rettij built-in components directory.
        :param mermaid_export_path: (Optional) Path for the mermaid topology export file.
        :param step_size: (Optional) Simulation step size, which is the simulation time increment for each step. Default: `1`.
        :param callback: (Optional) Callback function to be run when the simulation configuration has been parsed.
        :param rettij_instance: (Optional) Existing rettij instance. If supplied, it is used instead of creating a new one with the other parameters. This must be used when running other Mosaik simulators inside rettij, as rettij needs to fully run before the Mosaik models can be created.
        :param time_resolution: Mosaik time resolution. Default: `1.0`.
        :return: Mosaik meta model.
        """
        self.meta = super().init(
            sid,
            topology_path,
            file_loglevel,
            console_loglevel,
            fail_on_step_error,
            monitoring_config_path,
            sequence_path,
            kubeconfig_path,
            components_dir_path,
            mermaid_export_path,
            step_size,
            callback,
            rettij_instance,
            time_resolution,
        )

        # collect available attributes of rettij nodes (they are defined in the topology file under the "data" key)
        node_data_attributes = set()  # use a set because it can't hold the same item twice
        for node_id, node in self.rettij.nodes.items():
            try:
                node_data_attributes.update(node.mosaik_data.keys())  # insert keys into list
            except AttributeError:
                continue
        self.meta["models"]["node"]["attrs"].extend(list(node_data_attributes))

        return self.meta

    def create(self, num: int, model: str, **model_params: Any) -> List[Dict]:
        """
        Create the SimRettij mosaik simulator and the rettij simulation.

        Implements the mosaik `create()` method.

        Known limitations: Only one rettij model can be created (there was no use-case for multiple ones, yet).

        :param num: Number of instances to create.
        :param model: Model to create.
        :param model_params: Model parameters (meta['models'][model]['params']).
        :return: Return list with entitiy dicts created for the simulation.
        """
        if not self.rettij:
            raise RuntimeError("rettij not initialized, please run 'init()' first!")

        if num > 1 or self.eid:
            raise RuntimeError("Can only create one instance of rettij.")

        self.eid = f"{model}-0"

        self.rettij.create()

        # TODO this following logic should maybe be refactored be just use the
        # parent rettij create() method from rettij 1.2.0 onwards
        children: List[Dict] = self._create_children()
        model_instances: List[Dict] = [
            {
                "eid": self.eid,
                "type": model,
                "children": children,
            }
        ]

        # for node_id, node in self.rettij.nodes.items():
        #     node.executor.expose_port(1234, 6312)

        return model_instances

    def _create_children(self) -> List:
        children: List = []

        for node_eid in self.rettij.sm.nodes:
            node_entity = self._create_entity(node_eid, "node")
            children.append(node_entity)

        self.__actuator_factory = ActuatorFactory(self.rettij)
        self.__sensor_factory = SensorFactory(self.rettij)

        # create a list of actuator and sensor eids
        actuator_eids: List[str] = self.__actuator_factory.eids
        sensor_eids: List[str] = self.__sensor_factory.eids

        # append actuators and sensors to node entities created by superclass
        for node_entity in children:
            for actuator_eid in actuator_eids[node_entity["eid"]]:
                actuator = self.__actuator_factory.get(actuator_eid)
                actuator_entity = self._create_entity(actuator_eid, actuator.get_type())
                node_entity["children"].append(actuator_entity)

            for sensor_eid in sensor_eids[node_entity["eid"]]:
                sensor = self.__sensor_factory.get(sensor_eid)
                sensor_entity = self._create_entity(sensor_eid, sensor.get_type())
                node_entity["children"].append(sensor_entity)

        return children

    def _create_entity(self, eid: str, entity_type: str) -> Dict:
        """
        Create a single mosaik entity, which basically is a dict with some essential predefined fields.

        :param eid: entity id of the new entity
        :param entity_type: string with entity type like "node", "actuator" or "sensor"
        :return: new entity object (basically a a dict with some mandatory fields)
        """
        possible_entity_types = self.meta["models"].keys()
        if entity_type not in possible_entity_types:
            logger.error(f"Invalid entity type '{entity_type}', must be one of {possible_entity_types}.")
            sys.exit(1)

        entity = {
            "eid": eid,
            "type": entity_type,
            "children": [],
            "rel": [],  # relations are empty for now; not sure if this would really help anywhere
        }
        return entity

    def connect(self, source_node_id, target_node_id, **kwargs: Any) -> None:
        """
        Connect one Node to another.

        :param source_node_id: Base Node that the connection is initiated on
        :param target_node_id: Node to connect the base Node to
        :param kwargs: Custom parameters. Contents depend on the specific implementation.
        """
        rettij_source_node: Node = self.rettij.nodes.get(source_node_id)
        rettij_target_node: Node = self.rettij.nodes.get(target_node_id)

        super().connect(rettij_source_node, rettij_target_node, **kwargs)

    def get_actuator(self, actuator_eid) -> AbstractInterface:
        return self.__actuator_factory.get(actuator_eid)

    def get_sensor(self, sensor_eid) -> AbstractInterface:
        return self.__sensor_factory.get(sensor_eid)

    def step(self, sim_time: int, inputs: Dict[str, Any], max_advance: int) -> int:
        """
        Run a single step.

        Will always sleep for one second after executing the step to account for data propagation through the simulated real-time network.
        :param sim_time: Current simulation time, i.e. at which the step should be simulated with.
        :param inputs: Inputs to the step. Example: `{'n305': {'closed': {'SimPowerSwitch-0.Model_power_switch_0': True}}}`
        :param max_advance: Required by Mosaik API, not used internally.
        :return: Next simulation time that rettij.step() should be called at.
        """
        logger.debug(f"At step {sim_time} received inputs: {inputs}")

        # map the input attributes to the actual nodes
        actuator_eids = []

        for eid, attrs in inputs.items():
            for attr, src_ids in attrs.items():
                # inputs is like {'n305': {'closed': {'SimPowerSwitch-0.Model_power_switch_0': True}}}
                attr_value_dict = src_ids
                attr_value = attr_value_dict[list(attr_value_dict.keys())[0]]

                actuator = self.__actuator_factory.get(eid)
                if actuator:
                    actuator.execute(attr_value)
                    actuator_eids.append(eid)

        for eid in actuator_eids:
            # deleting actuator from inputs so rettij doesn't parse it again (it doesn't know about actuators anyways)
            inputs.pop(eid)

        for node_id, attrs in inputs.items():
            try:
                values_json: str = json.dumps(attrs)
            except TypeError as e:
                types = {k: type(v) for k, v in attrs.items()}
                raise ValueError(
                    f"{e}. Make sure input is a Dict that doesn't contain non-JSON-serializable values. "
                    f"Value: {attrs}. Types: {types}"
                )
            self.rettij.nodes[node_id].executor.http_post_to_node(6312, values_json)

        inputs = {}  # reset inputs, so rettij doesn't write them to files (it's handled above in a more efficient way)

        return super().step(sim_time, inputs, max_advance)

    def get_data(self, outputs):
        # outputs looks like this: {'n305': ['closed']})
        data = {}  # e.g. {"n305": {"closed": True}}
        data_from_rettij_nodes = {}  # data gathered from rettij nodes
        manipulate = False
        man_data = {}
        for eid, attrs in outputs.items():
            data[eid] = {}
            parsed_eid = InterfaceUtils.parse_eid(eid)
            if not parsed_eid:
                logger.error(f"Mosaik requests data for entity id '{eid}' which doesn't seem to exist.")
                sys.exit(1)
            if parsed_eid["interface_type"] == "sensor":
                sensor = self.__sensor_factory.get(eid)
                if "manipulation" in parsed_eid["interface_name"]:  # if manipulation sensor is found
                    manipulate = True
                    man_data = sensor.read()
                    data[eid]["value"] = 0  # hides manipulation data from retrieval
                else:
                    data[eid]["value"] = sensor.read()  # the sensor attribute name is always simply "value"
            elif parsed_eid["interface_type"] == "actuator":
                logger.error(f"You cannot request data from an actuator! (called eid: '{eid}')")
                sys.exit(1)
            else:
                # if the eid is for a node, not an actuator or a sensor, get data from rettij node directly
                node_data_str = self.rettij.nodes[eid].executor.http_get_from_node(6312)
                node_data = json.loads(node_data_str)
                node_data_filtered = {attr: node_data[attr] for attr in attrs}  # remove not requested attrs
                logger.debug(f"Output data for node {eid}: {node_data_filtered}")
                data_from_rettij_nodes[eid] = node_data_filtered

        if manipulate:
            data[man_data["target"]]["value"] = man_data["offset"]  # overrides specified data before returning

        data.update(data_from_rettij_nodes)

        logger.debug(f"Gathered outputs: {data}")
        return data
