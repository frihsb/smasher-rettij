import setuptools

setuptools.setup(
    name="smasher_rettij",
    version="0.1.14",
    description="Integration of rettij components into smasher",
    url="https://gitlab.com/frihsb/smasher-rettij",
    author="Forschungsgruppe Rechnernetze und Informationssicherheit",
    author_email="fri@hs-bremen.de",
    license="LGPL",
    packages=setuptools.find_packages(),
    include_package_data=True,
    python_requires='>=3.7.0',
    install_requires=["rettij>=1.6.0",
                      "mosaik>=3.0.0",
                      "mosaik-api>=3.0.0"
                      ],
    classifiers=[
        "Programming Language :: Python :: 3.7",
        "Development Status :: 1 - Planning",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: BSD License",
        "Operating System :: POSIX :: Linux",
        "Operating System :: MacOS :: MacOS X",
    ],
)
